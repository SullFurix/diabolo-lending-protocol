# Diabolo Lending protocol
## _Published on Solana Season Hackathon_

[![N|Solid](https://dev.gleizes.dev/wp/wp-content/uploads/2021/04/logotype.svg)](https://diabolo.io/)

Diabolo team aims to create a decentralized lending platform,
and make a new possibility for traders and investor, like the AAVE system.

## Who is diabolo trading ?

As diabolo [team] writes on the [diabolo website][diabolo]

> Diabolo (Diabolo.io) is a French startup
> freshly installed in Switzerland wishing 
> to disrupt the crypto-trading sector by
> developing  a complete painkiller ecosystem,
> articulated around 4 products CeFi and DeFi 
> whose main solution will allow the
> tokenization of traders’ performances
> using Solana and Serum technologies

We have built a centralized copytrading platform (diabolotrading.com)
and we are developing our decentralized copytrading system.

## Features

- Deposit on protocol to increase his collateral or earn interest
- Withdraw his deposit or recover the interest generated
- Borrow part of the collateral deposited
- Repay a borrow to recover its collateral
- Make a flash loan

Objective for our team is therefore to create a platform 
that can support all these functionalities

## Tech

Diabolo Lending System: 

[![N|Solid](https://gitlab.com/SullFurix/diabolo-lending-protocol/-/raw/master/Untitled_Diagram.png)](https://diabolo.io/)

Diabolo will use to create this platform:

- [ReactJS] - HTML enhanced for web apps!
- [node.js] - evented I/O for the backend
- [Solana] - Solana Blockchain
- [Serum] - Serum project
- [Lending program] - Lending program made by Solana team
- [Rust] - language to create Solana smart contract
- [Solana Flux Aggregator] - Oracle price

   [team]: <https://diabolo.io/>
   [diabolo]: <https://diabolo.io/>
   [node.js]: <http://nodejs.org>
   [ReactJS]: <https://reactjs.org/>
   [Solana]: <https://github.com/solana-labs/>
   [Serum]: <https://github.com/project-serum>
   [Lending program]: <https://github.com/solana-labs/solana-program-library>
   [Rust]: <https://github.com/solana-labs/rust>
   [Solana Flux Aggregator]: <https://github.com/octopus-network/solana-flux-aggregator>
